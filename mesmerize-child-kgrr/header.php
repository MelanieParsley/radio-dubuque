<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page-top" class="header-top">

	<div class="header-top-bar ">
		<div class="">
			<div class="header-top-bar-inner row middle-xs start-xs ">
				<div class="header-top-bar-area  col-xs area-left">
					<div class="top-bar-field" data-type="group" data-dynamic-mod="true">
						<i class="fa fa-map-marker"></i><span>1055 University Ave, Dubuque, IA 52001</span>
					</div>
					<div class="top-bar-field" data-type="group" data-dynamic-mod="true">
						<i class="fa fa-phone"></i><span><a href="tel:5636907625">563-690-7625</a></span>
					</div>
				</div>
				<div class="header-top-bar-area  col-xs-fit area-right">
					<div data-type="group" data-dynamic-mod="true" class="top-bar-social-icons">
						<a href="https://www.facebook.com/973TheRock" target="_blank"><img title="Facebook" alt="Facebook" src="https://973therock.radiodubuque.com/wp-content/themes/mesmerize-child-kgrr/images/facebook.png"></a>
						<a href="https://twitter.com/973therock" target="_blank"><img title="Twitter" alt="Twitter" src="https://973therock.radiodubuque.com/wp-content/themes/mesmerize-child-kgrr/images/twitter.png"></a>
						<a href="https://973therock.radiodubuque.com/feed/" target="_blank"><img title="RSS Feed" alt="RSS Feed" src="https://973therock.radiodubuque.com/wp-content/themes/mesmerize-child-kgrr/images/rss.png"></a>
						<a href="mailto:kgrr@973therock.com" target="_blank"><img title="Email" alt="Email" src="https://973therock.radiodubuque.com/wp-content/themes/mesmerize-child-kgrr/images/email.png"></a>
						<a href="https://973therock.radiodubuque.com/delays-and-cancellations"><img src="https://973therock.radiodubuque.com/wp-content/uploads/sites/4/2019/04/Delays-and-Cancellations.png" alt="Delays and Cancellations"></a>
						<a href="http://us7.maindigitalstream.com/3572/" target="_blank"><img src="https://973therock.radiodubuque.com/wp-content/uploads/sites/4/2016/02/listen-live.png" alt="Listen Live"></a>
						<!---<a href="http://lightningstream.com/Player.aspx?call=kgrr" target="_blank"><img src="https://973therock.radiodubuque.com/wp-content/uploads/sites/4/2016/02/listen-live.png" alt="Listen Live"></a>--->
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php mesmerize_get_navigation(); ?>
</div>

<div id="page" class="site">
    <div class="header-wrapper">
        <div <?php echo mesmerize_header_background_atts(); ?>>
            <?php do_action( 'mesmerize_before_header_background' ); ?>
			<?php mesmerize_print_video_container(); ?>
					<?php mesmerize_print_inner_pages_header_content(); ?>
            <?php mesmerize_print_header_separator(); ?>
        </div>
    </div>
