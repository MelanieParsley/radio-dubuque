<?php
/*
Template Name: Dubuque & Tri-State News
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <?php
                    while (have_posts()) : the_post();
                        the_content(); ?>
					<?php endwhile; ?>							
					
					<?php
						global $switched;
						switch_to_blog(2); //switched to blog id 2 (KDTH)
						
						// Get latest Post
						$latest_posts = get_posts('category=3&numberposts=0');
						$cnt =0;
					?> 
                    <div class="post-list row" <?php mesmerize_print_blog_list_attrs(); ?>>
						<?php foreach($latest_posts as $post) : setup_postdata($post); ?>
						<div class="<?php mesmerize_print_archive_entry_class(); ?>" data-masonry-width="<?php mesmerize_print_masonry_col_class(true); ?>">
							<div id="post-<?php the_ID(); ?>" <?php post_class('blog-post card '); ?>>
								<div class="post-content">
									<div class="col-xs-12 col-padding col-padding-xs">
										<h1 class="title"><?php echo $post->post_title; ?></h1>
										<p><span class="span12"><?php echo the_time( get_option( 'date_format' ) ); ?></span></p>
										<?php echo apply_filters( 'the_content', $post->post_content ); ?>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach ; ?>
					</div>
					<?php restore_current_blog(); //switched back to main site ?>
                </div>

                <!---<div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php //mesmerize_get_sidebar('pages'); ?>
                </div>--->
            </div>
        </div>
    </div>
<?php get_footer(); ?>
