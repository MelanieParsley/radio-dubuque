<?php
/*
Template Name: Mother's Day
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <?php
                    while (have_posts()) : the_post();
                        the_content(); ?>
                    <?php endwhile; ?>
					<p>&nbsp;</p>
					<h3>Read other peoples' greetings!</h3>
					
					<?php
						global $switched;
						switch_to_blog(2); //switched to blog id 2 (KDTH)
						
						// Get latest Post
						$latest_posts = get_posts('category=12&numberposts=1000');
						$cnt =0;
					?> 
						<?php foreach($latest_posts as $post) : setup_postdata($post); ?>
							<div class="greetings kdth" style="padding: 10px;">
							<?php echo apply_filters( 'the_content', $post->post_content ); ?>
							<?php echo $post->post_excerpt; ?><br style="clear: both;" />
							</div>
						<?php endforeach ; ?>
					 
					<?php restore_current_blog(); //switched back to main site ?>
					
					<?php
						global $switched;
						switch_to_blog(3); //switched to blog id 3 (KATF)
						
						// Get latest Post
						$latest_posts = get_posts('category=4&numberposts=1000');
						$cnt =0;
					?> 
						<?php foreach($latest_posts as $post) : setup_postdata($post); ?>
							<div class="greetings katf" style="padding: 10px;">
							<?php echo apply_filters( 'the_content', $post->post_content ); ?>
							<?php echo $post->post_excerpt; ?><br style="clear: both;" />
							</div>
						<?php endforeach ; ?>
					 
					<?php restore_current_blog(); //switched back to main site ?>
					
					<?php
						global $switched;
						switch_to_blog(4); //switched to blog id 4 (KGRR)
						
						// Get latest Post
						$latest_posts = get_posts('category=5&numberposts=1000');
						$cnt =0;
					?> 
						<?php foreach($latest_posts as $post) : setup_postdata($post); ?>
							<div class="greetings kgrr" style="padding: 10px;">
							<?php echo apply_filters( 'the_content', $post->post_content ); ?>
							<?php echo $post->post_excerpt; ?><br style="clear: both;" />
							</div>
						<?php endforeach ; ?>
					 
					<?php restore_current_blog(); //switched back to main site ?>
					
					<?php
						global $switched;
						switch_to_blog(5); //switched to blog id 5 (WVRE)
						
						// Get latest Post
						$latest_posts = get_posts('category=4&numberposts=1000');
						$cnt =0;
					?> 
						<?php foreach($latest_posts as $post) : setup_postdata($post); ?>
							<div class="greetings wvre" style="padding: 10px;">
							<?php echo apply_filters( 'the_content', $post->post_content ); ?>
							<?php echo $post->post_excerpt; ?><br style="clear: both;" />
							</div>
						<?php endforeach ; ?>
					 
					<?php restore_current_blog(); //switched back to main site ?>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
