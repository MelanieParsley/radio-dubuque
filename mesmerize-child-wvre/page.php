<?php mesmerize_get_header(); ?>

    <div class="page-content">
        <div class="<?php mesmerize_page_content_wrapper_class(); ?>">
            
            <?php if (is_page( 125 )) { ?>
            
                <div id="events-content" oncopy="return false" oncut="return false" onpaste="return false">
                <!-- Disable right click code-->
                <script language="JavaScript"> 
                <!--
                var message="Copyright \u00A9 1011theriver.com. All Rights Reserved.";
                document.oncontextmenu = disableRightClick;
                
                function disableRightClick()
                {
                  alert(message);
                  return false;
                }	
                // --> 
                </script>
                
                <!--End disable right click code-->
            
            <?php } ?>
            <?php
            while (have_posts()) : the_post();
                get_template_part('template-parts/content', 'page');
            endwhile;
            ?>
            
            <?php if (is_page( 125 )) { ?>
            	</div>
            <?php } ?>
        </div>
    </div>

<?php get_footer(); ?>
