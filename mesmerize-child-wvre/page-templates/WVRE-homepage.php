<?php
/*
Template Name: WVRE Homepage
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
					<?php if ( is_front_page() ) {
						newannouncement( $group = "GROUP1" );
					} ?>
                    <?php
                    while (have_posts()) : the_post();
                        the_content(); ?>
					<?php endwhile; ?>
						
						<?php
						
						// Get a SimplePie feed object from the specified feed source.
					$rss = fetch_feed( 'http://musicnews-country.franklymedia.com/cps/today.full.rss?call=wvre&service=countrynow' );
					
					if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
					
						// Figure out how many total items there are, but limit it to 5. 
						$maxitems = $rss->get_item_quantity( 5 ); 
					
						// Build an array of all the items, starting with element 0 (first element).
						$rss_items = $rss->get_items( 0, $maxitems );
					
					endif;
					?>
					
					<div class="musicnews">
						<h2>Country Music At Large</h2>
					
						<?php if ( $maxitems == 0 ) : ?>
						<p><?php _e( 'No items', 'my-text-domain' ); ?></p>
						<?php else : ?>
							<?php // Loop through each feed item and display each item as a hyperlink. ?>
							<?php foreach ( $rss_items as $item ) : ?>
								<h3><?php echo esc_html( $item->get_title() ); ?></h3>
								 <p>
								 <?php if ($enclosure = $item->get_enclosure())
								{ ?>
									<img src="<?php echo $enclosure->get_thumbnail(); ?>" width="200" class="alignleft" style="padding-right: 10px;" />
								<?php } ?>
								 <?php echo $item->get_content(); ?></p>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
