<?php
/*
Template Name: Community Calendar
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <?php
                    //while (have_posts()) : the_post();
                        //the_content(); ?><!---<br style="clear: both;" />--->
                    <?php //endwhile; ?>

                    <h3><a href="https://1011theriver.radiodubuque.com/to-do-list/submit-event/">Submit Your Own Event</a></h3>
                    <h2>On-going Events</h2>
			
					<?php
						global $switched;
						switch_to_blog(3); //switched to blog id 3 (KATF)
						
						// Get latest Post
						$latest_posts = get_posts('category=11&numberposts=0');
						$cnt =0;
					?> 
						<?php foreach($latest_posts as $post) : setup_postdata($post); ?>
							<!---<h2><?php //echo $post->post_title; ?></h2>--->
							<?php echo apply_filters( 'the_content', $post->post_content ); ?><br style="clear: both;" />
						<?php endforeach ; ?>
					 
					<?php restore_current_blog(); //switched back to main site ?>
                    
                    <h2>Upcoming Events</h2>
                    <?php echo do_shortcode( '[events_list blog="2,3,4,5"]' ); ?>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
