<?php mesmerize_get_header(); ?>
    <div class="content post-page">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <div class="post-item">
						<?php
						if ( have_posts() ):
							while ( have_posts() ):
								the_post();
								get_template_part( 'template-parts/content', 'single' );
							endwhile;
						else :
							get_template_part( 'template-parts/content', 'none' );
						endif;
						?>
                    </div>
                </div>
				<div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
					<?php mesmerize_get_sidebar('pages'); ?>
				</div>
            </div>
        </div>

    </div>
<?php get_footer(); ?>
