<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page-top" class="header-top">

	<div class="header-top-bar ">
		<div class="">
			<div class="header-top-bar-inner row middle-xs start-xs ">
				<div class="header-top-bar-area  col-xs area-left">
					<div class="top-bar-field" data-type="group" data-dynamic-mod="true">
						<i class="fa fa-map-marker"></i><span>1055 University Ave, Dubuque, IA 52001</span>
					</div>
					<div class="top-bar-field" data-type="group" data-dynamic-mod="true">
						<i class="fa fa-phone"></i><span><a href="tel:5636901370">563-690-1370</a></span>
					</div>
				</div>
				<div class="header-top-bar-area  col-xs-fit area-right">
					<div data-type="group" data-dynamic-mod="true" class="top-bar-social-icons">
						<a href="https://www.facebook.com/AM-1370-KDTHThe-Voice-of-the-Tri-States-403149500489/" target="_blank"><img title="Facebook" alt="Facebook" src="https://kdth.radiodubuque.com/wp-content/themes/mesmerize-child-kdth/images/facebook.png"></a>
						<a href="https://twitter.com/AM1370KDTH" target="_blank"><img title="Twitter" alt="Twitter" src="https://kdth.radiodubuque.com/wp-content/themes/mesmerize-child-kdth/images/twitter.png"></a>
						<a href="https://kdth.radiodubuque.com/feed/" target="_blank"><img title="RSS Feed" alt="RSS Feed" src="https://kdth.radiodubuque.com/wp-content/themes/mesmerize-child-kdth/images/rss.png"></a>
						<a href="mailto:kdth@kdth.com" target="_blank"><img title="Email" alt="Email" src="https://kdth.radiodubuque.com/wp-content/themes/mesmerize-child-kdth/images/email.png"></a>
						<a href="https://kdth.radiodubuque.com/delays-and-cancellations"><img src="https://kdth.radiodubuque.com/wp-content/uploads/sites/2/2019/04/Delays-and-Cancellations.png" alt="Delays and Cancellations"></a>
						<a href="http://us7.maindigitalstream.com/3568/" target="_blank"><img src="https://kdth.radiodubuque.com/wp-content/uploads/sites/2/2016/01/listen-live.png" alt="Listen Live"></a>
						<!---<a href="http://lightningstream.com/Player.aspx?call=kdth" target="_blank"><img src="https://kdth.radiodubuque.com/wp-content/uploads/sites/2/2016/01/listen-live.png" alt="Listen Live"></a>--->
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php mesmerize_get_navigation(); ?>
</div>

<div id="page" class="site">
    <div class="header-wrapper">
        <div <?php echo mesmerize_header_background_atts(); ?>>
            <?php do_action( 'mesmerize_before_header_background' ); ?>
			<?php mesmerize_print_video_container(); ?>
					<?php mesmerize_print_inner_pages_header_content(); ?>
            <?php mesmerize_print_header_separator(); ?>
        </div>
    </div>
