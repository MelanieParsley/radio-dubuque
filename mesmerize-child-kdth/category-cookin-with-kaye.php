<?php mesmerize_get_header(); ?>

    <div class="content blog-page">
        <div class="gridContainer <?php mesmerize_page_content_wrapper_class(); ?>">
            <div class="row">
                <div class="col-xs-12 <?php mesmerize_posts_wrapper_class(); ?>">
                    <div class="row" <?php //mesmerize_print_blog_list_attrs(); ?>>
                        <?php
                        if (have_posts()):
							$count = 0;
                            while (have_posts()):
                                the_post();
							$count++;

							if ($count <= 1) : ?>
						
							<h2><a class="permalink" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
							<?php the_title(); ?> (<?php the_date('', '', '', TRUE);?>)</a></h2>
							<?php if(function_exists('pf_show_link')){echo pf_show_link();} ?>
							
							<?php the_content();  // display the full content of the first post only ?>
							
							<p align="center"><img src="https://kdth.radiodubuque.com/wp-content/uploads/sites/2/2016/03/cremers.png" style="padding-right: 40px;" /> &nbsp; <img src="https://kdth.radiodubuque.com/wp-content/uploads/sites/2/2017/04/Vans-Liquor-Store.jpg" style="vertical-align: top; padding-right: 40px;" /> &nbsp; <img src="https://kdth.radiodubuque.com/wp-content/uploads/sites/2/2016/03/Happee-Smith-Productions.jpg" /></p>
							
							<h2>Past Recipes</h2>
						
							<?php else : ?>
						
							<p class="recipe-title"><?php the_date('', '', '', TRUE);?> - <a class="permalink" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>
							<?php // Just the permalinks
						
							endif;

						endwhile;
                        else:
                            get_template_part('template-parts/content', 'none');
                        endif;
                        ?>
                    </div>
                    <div class="navigation-c">
                        <?php
                        if (have_posts()):
                            mesmerize_print_pagination();
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
