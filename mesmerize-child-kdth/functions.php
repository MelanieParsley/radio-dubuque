<?php

function prefix_category_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'prefix_category_title' );

add_filter( 'wp_feed_cache_transient_lifetime', create_function( '$a', 'return 1800;' ) );


function weekend_sports() {
						
    // Get a SimplePie feed object from the specified feed source.
    $rss = fetch_feed( 'http://kdt2.vncnews.com/' );

    if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly

    // Figure out how many total items there are, but limit it to 20. 
    $maxitems = $rss->get_item_quantity( 20 ); 

    // Build an array of all the items, starting with element 0 (first element).
    $rss_items = $rss->get_items( 0, $maxitems );

    endif;

    ?>

    <h2 class="title">Weekend Sports Headlines</h2>
                
    <?php if ( $maxitems == 0 ) : ?>
    <p><?php _e( 'No items', 'my-text-domain' ); ?></p>
    <?php else : ?>
        <ul class="display-posts-listing">
        <?php // Loop through each feed item and display each item as a hyperlink. ?>
        <?php foreach ( $rss_items as $item ) : ?>
            <li class="listing-item">
                <strong><span class="title"><?php echo esc_html( $item->get_title() ); ?></span></strong>
                <span class="date">(<?php echo $item->get_date('m/d/Y'); ?>)</span>
                <span class="excerpt-dash">-</span>
                <span class="excerpt"><?php echo $item->get_content(); ?></span>
            </li>
        <?php endforeach; ?>
        </ul>
    <?php endif; ?>
<?php }

add_shortcode('displayWeekendSports', 'weekend_sports');
