<?php
/*
Template Name: Dubuque & Tri-State News
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <?php
                    while (have_posts()) : the_post();
                        the_content(); ?>
					<?php endwhile; ?>

					<?php 
					//RSS Fetching with WordPress' fetch_feed
					//https://gist.github.com/elg0nz/291384/f7aadfdffcd03fccda7429e21970104f46b2c3bf
					?>
					
					<?php
						// Get a SimplePie feed object from the specified feed source.
						$rss = fetch_feed( 'http://kdth.vncnews.com/' );
											
						if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly

							// Figure out how many total items there are, but limit it to 50. 
							$maxitems = $rss->get_item_quantity( 50 ); 

							// Build an array of all the items, starting with element 0 (first element).
							$rss_items = $rss->get_items( 0, $maxitems );

						endif;
					?> 
                    <div class="row">
						<?php foreach($rss_items as $item) : ?>
						
							<div <?php post_class('blog-post '); ?>>
								<div class="post-content">
									<div class="col-xs-12 col-padding col-padding-xs">
										<h1 class="title"><?php echo esc_html( $item->get_title() ); ?></h1>
										<p><span class="span12"><?php echo $item->get_date('F j, Y'); ?></span></p>
										<?php echo $item->get_content(); ?>
									</div>
								</div>
							</div>
						<?php endforeach ; ?>
					</div>
                </div>

                <!---<div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php //mesmerize_get_sidebar('pages'); ?>
                </div>--->
            </div>
        </div>
    </div>
<?php get_footer(); ?>
