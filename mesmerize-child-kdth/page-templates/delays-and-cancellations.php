<?php
/*
Template Name: Delays and Cancellations
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <?php
                    while (have_posts()) : the_post();
                        //the_content(); ?>
						<div class="delay-sponsors"><a href="https://www.facebook.com/beidler.towing/" target="_blank"><img class="alignleft size-full" src="https://973therock.radiodubuque.com/wp-content/uploads/sites/4/2019/09/Beidler-Towing.jpg" alt="Beidler Towing" width="200" height="165" /></a> <a href="https://www.theisens.com/" target="_blank"><img class="alignleft size-full" src="https://973therock.radiodubuque.com/wp-content/uploads/sites/4/2017/12/Theisens.jpg" alt="Theisens" width="200" height="165" /></a> <a href="http://www.toysdonerightbodyshop.com/"><img class="alignleft size-full" src="https://973therock.radiodubuque.com/wp-content/uploads/sites/4/2016/03/Toys-Done-Right.jpg" alt="Toys Done Right" width="200" height="165" /></a></div><br style="clear: left;" />
                    <?php endwhile; ?>
					<?php
						global $switched;
						switch_to_blog(4); //switched to blog id 4 (KGRR)
						
						// Get latest Post
						$latest_posts = get_posts('category=4&numberposts=0');
						$cnt =0;
					?> 
						<?php foreach($latest_posts as $post) : setup_postdata($post); ?>
							<?php echo apply_filters( 'the_content', $post->post_content ); ?><br style="clear: both;" />
						<?php endforeach ; ?>
					 
					<?php restore_current_blog(); //switched back to main site ?>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
