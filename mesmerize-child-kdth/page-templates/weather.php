<?php
/*
Template Name: Weather
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <?php
                    while (have_posts()) : the_post();
                        the_content(); ?>
					<?php endwhile; ?>							
					
					<?php
						global $switched;
						switch_to_blog(2); //switched to blog id 2 (KDTH)
						
						// Get latest Post
						$latest_posts = get_posts('category=10&numberposts=0');
						$cnt =0;
					?> 
						<?php foreach($latest_posts as $post) : setup_postdata($post); ?>
							<h2><?php echo $post->post_title; ?></h2>
							<?php echo apply_filters( 'the_content', $post->post_content ); ?><br style="clear: both;" />
						<?php endforeach ; ?>
					 
					<?php restore_current_blog(); //switched back to main site ?>
						
						<p><a href="http://www.kcrg.com/weather" target="_blank"><img src="http://gray.ftp.clickability.com/kcrgwebftp/Radio_Dubuque_PPSN.jpg" alt="" width="500" /></a></p>
						<p><a href="http://www.kcrg.com/weather" target="_blank"><img src="http://gray.ftp.clickability.com/kcrgwebftp/Radio_Dubuque_Planner.jpg" alt="" width="500" /></a></p>
						<p><a href="http://www.kcrg.com/weather/pinpointradar/" target="_blank"><img src="http://gray.ftp.clickability.com/kcrgwebftp/Radio_Dubuque_Satrad.jpg" alt="" width="500" /></a></p>
						
						<?php
						
						// Get a SimplePie feed object from the specified feed source.
					$rss = fetch_feed( 'http://www.kcrg.com/templates/2015_XML_FEED?cat=Weather%20Water%20Cooler&placement=/content/news' );
					
					if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
					
						// Figure out how many total items there are, but limit it to 5. 
						$maxitems = $rss->get_item_quantity( 5 ); 
					
						// Build an array of all the items, starting with element 0 (first element).
						$rss_items = $rss->get_items( 0, $maxitems );
					
					endif;
					?>
					
					<h2><a href="http://www.kcrg.com/weather/weatherwatercooler/" target="_blank">KCRG Weather Blog</a></h2>
					
						<?php if ( $maxitems == 0 ) : ?>
						<p><?php _e( 'No items', 'my-text-domain' ); ?></p>
						<?php else : ?>
							<?php // Loop through each feed item and display each item as a hyperlink. ?>
							<?php foreach ( $rss_items as $item ) : ?>
								<h3><a href="<?php echo esc_url( $item->get_permalink() ); ?>" target="_blank"><?php echo esc_html( $item->get_title() ); ?></a></h3>
								 <p><?php echo $item->get_content(); ?></p>
							<?php endforeach; ?>
						<?php endif; ?>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
