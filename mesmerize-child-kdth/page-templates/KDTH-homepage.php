<?php
/*
Template Name: KDTH Homepage
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
					<?php if ( is_front_page() ) {
						newannouncement( $group = "GROUP1" );
					} ?>
                    <?php
                    while (have_posts()) : the_post();
                        the_content(); ?>
					<?php endwhile; ?>
						
						<?php
						
						// Get a SimplePie feed object from the specified feed source.
					    $rss = fetch_feed( 'http://kdth.vncnews.com/' );
					
					    if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
					
						// Figure out how many total items there are, but limit it to 5. 
						$maxitems = $rss->get_item_quantity( 5 ); 
					
						// Build an array of all the items, starting with element 0 (first element).
						$rss_items = $rss->get_items( 0, $maxitems );
					
					    endif;
					    ?>
					
                    <h2 class="title"><span style="text-decoration: underline;"><a href="https://kdth.radiodubuque.com/dubuque-tri-state-news/">Dubuque &amp; Tri-State News</a></span></h2>
                
                    <?php if ( $maxitems == 0 ) : ?>
                    <p><?php _e( 'No items', 'my-text-domain' ); ?></p>
                    <?php else : ?>
                        <ul class="display-posts-listing">
                        <?php // Loop through each feed item and display each item as a hyperlink. ?>
                        <?php foreach ( $rss_items as $item ) : ?>
                            <li class="listing-item">
                                <strong><span class="title"><?php echo esc_html( $item->get_title() ); ?></span></strong>
                                <span class="date">(<?php echo $item->get_date('m/d/Y'); ?>)</span>
                                <span class="excerpt-dash">-</span>
                                <span class="excerpt"><?php echo $item->get_content(); ?></span>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <h2 class="title"><span style="text-decoration: underline;"><a href="https://kdth.radiodubuque.com/category/sports/">Sports</a></span></h2>
                    <?php echo do_shortcode ( '[display-posts category="sports" posts_per_page="4" include_date="true" include_excerpt="true"]' ) ?>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
