<div class="navigation-bar logo-above-menu <?php mesmerize_header_main_class() ?>" <?php mesmerize_navigation_sticky_attrs() ?>>
	<div class="logo-bannerads">
		<div class="header-left">
			<?php mesmerize_print_logo(); ?>
		</div>
		<div class="header-right">
			<?php if ( is_front_page() ) {
			  // static homepage
			  echo adrotate_group(1);
			} elseif ( in_category(3) || in_category(4) || in_category(5) || in_category(6) || is_page( 'Weather Forecast' ) ) {
			  // news pages
			   echo adrotate_group(2);
			} elseif ( is_page( 'Obituaries' ) ) {
			  // obituaries page
			   echo adrotate_group(3, 5);
			} else {
			  //everything else (pool)
			   echo adrotate_group(4);
			} ?>
		</div><!-- .header-right -->
	</div>
    <div class="navigation-wrapper <?php mesmerize_navigation_wrapper_class() ?>">
        <div class="row basis-auto between-xs">
	        <!---<div class="logo_col col-xs col-sm-12 center-sm">--->
	        <div class="main_menu_col col-xs-fit col-sm">
	            <?php mesmerize_print_primary_menu(); ?>
	        </div>
	    </div>
    </div>
</div>
