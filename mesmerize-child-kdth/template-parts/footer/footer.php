<div <?php echo mesmerize_footer_container('footer-simple') ?>>
    <div <?php echo mesmerize_footer_background('footer-content center-xs') ?>>
        <div class="gridContainer">
	        <div class="row middle-xs footer-content-row">
	            <div class="footer-content-col col-xs-12">
	                    <?php //echo mesmerize_get_footer_copyright(); ?>
					<p>&copy; <?php echo date('Y'); ?>  <a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a></p>
					<p><a href="
					<?php date_default_timezone_set('America/Chicago');
					$today = date("Y-m-d H:i:s");
					$date = date("2021-07-26 00:00:00");
					if ($date >= $today) { ?>
					https://radiodubuque.com/fcc_eeo/Radio-Dubuque-EEO-8-1-19-Thru-7-31-20.pdf <?php ; 
					}
					else { ?>
					https://radiodubuque.com/fcc_eeo/Radio-Dubuque-EEO-8-1-20-Thru-7-31-21.pdf <?php ;
					} ?>
					" target="_blank">Radio Dubuque EEO Public File</a></p>
					<p><a href="https://publicfiles.fcc.gov/am-profile/kdth" target="_blank">KDTH Public File</a></p>
	            </div>
	        </div>
	    </div>
    </div>
</div>
